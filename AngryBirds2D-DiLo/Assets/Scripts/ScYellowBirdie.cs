﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScYellowBirdie : ScBird
{
   [SerializeField] private float _boostforce = 100f;
   public bool _hasBoost = false;

   public void Boost(){
       if (State == BirdState.Thrown && !_hasBoost)
       {
           _rb.AddForce(_rb.velocity.normalized * _boostforce,ForceMode2D.Impulse);
           _hasBoost = true;
       }
   }

    public override void OnTap()
    {
        Boost();
    }
}
