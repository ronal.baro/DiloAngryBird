﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScTrailController : MonoBehaviour
{
    public GameObject _prefabTrail;
    internal ScBird _targetBird;
    public List<GameObject> _trails = new List<GameObject>();

    public void SetBird(ScBird bird)
    {
        _targetBird = bird;
        for (int i = 0; i < _trails.Count; i++)
        {
            Destroy(_trails[i].gameObject);
        }
        _trails.Clear();

    }

    public IEnumerator SpawnTrail()
    {
        _trails.Add(Instantiate(_prefabTrail, _targetBird.transform.position, Quaternion.identity));
        yield return new WaitForSeconds(0.1f);
        if (_targetBird != null && _targetBird.State != ScBird.BirdState.HitSomething)
        {
            StartCoroutine(SpawnTrail());
        }
    }

}
