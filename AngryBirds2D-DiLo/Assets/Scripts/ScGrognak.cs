﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScGrognak : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        string tag = other.gameObject.tag;
        if (tag == "Bird" || tag == "Enemy" || tag == "Obstacle")
        {
            StartCoroutine(DestroyAfter(2, other.gameObject));
        }
    }

    IEnumerator DestroyAfter(float s, GameObject gO)
    {
        yield return new WaitForSeconds(s);
        if (gO.tag == "Enemy")
        {
            gO.SetActive(false);
        }
        else
            Destroy(gO);
    }
}
