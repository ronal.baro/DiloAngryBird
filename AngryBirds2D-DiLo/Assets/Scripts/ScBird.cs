﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScBird : MonoBehaviour
{   public enum BirdState {Idle ,Thrown, HitSomething};
    public GameObject _parent;
    public Rigidbody2D _rb;

    public UnityAction OnBirdDestroyed = delegate{};
    public UnityAction<ScBird> OnBirdShot = delegate{};
    private BirdState _state;
    public BirdState State {get{return _state;}}
    private float _minVelocity = 0.05f;
    private bool _flagDestroy = false; 
    void Start()
    {
        _rb.simulated = false ;
        _state = BirdState.Idle;
        
    }
    
    private void FixedUpdate() {
        if ((_state == BirdState.Thrown || _state == BirdState.HitSomething)  && _rb.velocity.sqrMagnitude <= _minVelocity && !_flagDestroy){
             _flagDestroy = true;
            StartCoroutine(DestroyAfter(2));
        }
    }

    private IEnumerator DestroyAfter(float second){
        yield return new WaitForSeconds(second);
        Destroy(gameObject);
        
    }

    public void MoveTo(GameObject newParent){
      
         transform.SetParent(newParent.transform); //YEEEAAHHH Orang tua baru!!!
        transform.localPosition = Vector3.zero;
    }
    public void Shoot(Vector2 dir , float forceMul, float distance){
        _state = BirdState.Thrown;
        _rb.simulated = true;
        
        _rb.AddForce(dir * ((forceMul * distance) + (distance*3)), ForceMode2D.Impulse);
        //distance*3 itu biar semakin jauh dari titik awal,, semakin besar multiplier nya... tapi kalau terlalu besar malah gak nyaman...
        OnBirdShot(this);
    }
    public virtual void OnHit(){
        Debug.Log("NoBakat1");
    }
    public virtual void OnTap(){
        Debug.Log("NoBakat2");
    }
    
    private void OnCollisionEnter2D(Collision2D other) {
        _state = BirdState.HitSomething;
        OnHit();
    }
     void OnDestroy() {
         if(_state == BirdState.Thrown|| _state == BirdState.HitSomething)
        OnBirdDestroyed();
 }


    
}
