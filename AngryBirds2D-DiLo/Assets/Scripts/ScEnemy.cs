﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class ScEnemy : MonoBehaviour
{
    public float health = 50f;
    public UnityAction<GameObject> OnEnemyDestroyed = delegate { };
    private bool _isHit = false;

    // private void OnDestroy() {
    //     if(_isHit){
    //         OnEnemyDestroyed(gameObject);
    //     }
    // }

    private void OnDisable()
    {
        if (_isHit)
        {
            OnEnemyDestroyed(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.GetComponent<Rigidbody2D>() == null) return; //karna enemy punya rigidbody+dynamic..(mungkin)

        if (other.gameObject.tag == "Bird")
        {
            _isHit = true;
            // Destroy(gameObject);
            gameObject.SetActive(false);
        }
        else if (other.gameObject.tag == "Obstacle")
        {
            float damage = other.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 10;
            health -= damage;
            Debug.Log(health);
            if (health <= 0)
            {
                _isHit = true;
                //  Destroy(gameObject);
                gameObject.SetActive(false);
            }
        }
    }
}
