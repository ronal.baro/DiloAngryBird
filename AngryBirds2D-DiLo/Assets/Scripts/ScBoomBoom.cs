﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScBoomBoom : ScBird
{
    [SerializeField] private float _boomForce = 100f;
    [SerializeField] private float _boomRadius = 5f;

    [SerializeField] GameObject _boomFX;
    public bool _hasBoom = false;

    public void Boom()
    {
        if ((State != BirdState.Idle) && !_hasBoom)
        {
            //ENEMIES
            Debug.Log("BOOOOMM!!!");
            ScEnemy[] enemies = GameObject.FindObjectsOfType<ScEnemy>();
            foreach (ScEnemy enemy in enemies)
            {
                float jarak = Vector3.Distance(enemy.gameObject.transform.position, this.transform.position);
                if (jarak > _boomRadius)
                {
                    continue;
                }
                Vector2 arah = (enemy.gameObject.transform.position - this.transform.position).normalized;
                enemy.GetComponent<Rigidbody2D>().AddForce((_boomRadius / jarak / 10) * _boomForce * arah, ForceMode2D.Impulse);
            }

            //OBSTACLES

            GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
            foreach (GameObject obstacle in obstacles)
            {
                float jarak = Vector3.Distance(obstacle.transform.position, this.transform.position);
                if (jarak > _boomRadius)
                {
                    continue;
                }
                Vector2 arah = (obstacle.gameObject.transform.position - this.transform.position).normalized;
                obstacle.GetComponent<Rigidbody2D>().AddForce((_boomRadius / jarak / 10) * _boomForce * arah, ForceMode2D.Impulse);
            }
            //_rb.AddForce(_rb.velocity.normalized * _boomForce,ForceMode2D.Impulse);
            GameObject boom = Instantiate(_boomFX,transform.position, Quaternion.identity);
            //_boomFX.transform.position = transform.position;
            boom.GetComponent<ParticleSystem>().Play();
              _hasBoom = true;
              StartCoroutine(DestroyFx(1,boom));
          
        }
    }
    IEnumerator DestroyFx(float s,GameObject boom){
        yield return  new WaitForSeconds(s);
         Destroy(boom);

    }
    public override void OnHit()
    {
        Boom();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, _boomRadius);
    }
}
