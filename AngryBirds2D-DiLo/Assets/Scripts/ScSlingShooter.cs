﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScSlingShooter : MonoBehaviour
{
    public Collider2D col;
    public LineRenderer Trajectory;
    private Vector2 _startPos;
    private Vector2 _mousePos;

    [SerializeField]
    private float _radius = 0.75f;
    [SerializeField]   
     private float _forceMultiplier = 30f;
    
    internal ScBird _bird; //internal biar gamecontroller bisa check ada tidaknya..jaga2 aja
    void Start()
    {
        _startPos = transform.position;
    }

    private void OnMouseDrag()
    {

        if (_bird == null)
        {
            return;
        }

        _mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = _mousePos - _startPos; // titik sekarang dibanding titik awal
        //aku pake magnitude ketmbang sqrMagnitude.. karena butuh lokasi pasti
        if (direction.magnitude > _radius)
        { // jika lebih dari radius kembalikan ke max radius yang sudah diset
            direction = direction.normalized * _radius;
        }
        transform.position = _startPos + direction;

        float distance = Vector2.Distance(_startPos, transform.position);

        if (Trajectory.enabled == false){
            Trajectory.enabled = true;
        }
        DisplayTrajectory(distance);
    }

    void DisplayTrajectory(float distance){
        if(_bird ==null){
            return;
        }
        Vector2 velocity = _startPos - (Vector2)transform.position;
        int segmentCount = 5;
        Vector2[] segments = new Vector2[segmentCount];

        segments[0] = transform.position;
        //(dir * ((forceMul * distance) + (distance*3)
         Vector2 segVelocity = velocity * _forceMultiplier * distance;

        //seharusnya ini disesuaikan dengan addfoce punya ku,, cuma kebetulan ga jauh beda.. ya asal tau aja
         for (int i = 1; i < segmentCount; i++)
        {
            float elapsedTime = i * Time.fixedDeltaTime * 5;
            segments[i] = segments[0] + segVelocity * elapsedTime + 0.5f * Physics2D.gravity * Mathf.Pow(elapsedTime, 2);
        }
        Trajectory.positionCount = segmentCount;
          for (int i = 0; i < segmentCount; i++)
        {
            Trajectory.SetPosition(i, segments[i]);
        }
    }

    private void OnMouseUp()
    {
        if (_bird == null)
        {
            return;
        }

        Vector2 arah = (_startPos - (Vector2)transform.position).normalized;
        //idk kenapa velocity, sedangkan sudah ada distance.. jadi kujadikan arah saja...

        float distance = Vector2.Distance(_startPos, transform.position);
        Debug.Log(distance);
        if (distance > 0.5f)
        { //jarak minimal shoot.. 
            _bird.Shoot(arah, _forceMultiplier, distance);
            _bird = null;
        }
        transform.position = _startPos;
        _mousePos = _startPos;
        Trajectory.enabled = false;
    }

    public void initiateBird(ScBird bird){
        _bird = bird;
        _bird.MoveTo(gameObject);
        //Collider.enabled = true; pake if aja lah ini collider
    }

}
