﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class ScGameController : MonoBehaviour
{
    public ScSlingShooter _slingShooter;
    public ScTrailController _trailController;
    public List<ScBird> _birds;
    public List<ScEnemy> _enemies = new List<ScEnemy>();

    [SerializeField] private Transform Enemies;
    private ScBird _curBird;

    private BoxCollider2D TapCollider;
    private bool _isGameEnded = false;
    int _enemyCounter;
    private bool _isWin = true;
    public GameObject EndGamePanel;
    public Text WinStatus;



    void Start()
    {
        TapCollider = GetComponent<BoxCollider2D>();
        foreach (Transform enemy in Enemies)
        {
            ScEnemy n = enemy.GetComponent<ScEnemy>();
            _enemies.Add(n);
        }
        _enemyCounter = _enemies.Count;

        foreach (ScBird bird in _birds)
        {
            bird.OnBirdDestroyed += ChangeBird;
            bird.OnBirdShot += AssignTrail;
        }
        SetBird();

        foreach (ScEnemy enemy in _enemies)
        {
            //   enemy.OnEnemyDestroyed += CheckGameEnd; sementara disable
        }
        TapCollider.enabled = false;
    }

    public void ChangeBird()
    {
        if (_slingShooter._bird != null || _isGameEnded)
        {
            return;

        }
        _birds[0].OnBirdDestroyed -= ChangeBird;

        _birds.RemoveAt(0);



        if (CheckGameEnd())   // check musuh dulu kalau habis lanjut
        {
            return;
        }

        if (_birds.Count > 0)
        {
            SetBird();

        }
        else
        {
            _isGameEnded = true;
            _isWin = false;
            ShowEndGamePanel();
        }
    }

    public void SetBird()
    {
        _slingShooter.initiateBird(_birds[0]);
        _curBird = _birds[0];
    }

    public void AssignTrail(ScBird bird)
    {
        TapCollider.enabled = true;
        _trailController.SetBird(bird);
        StartCoroutine(_trailController.SpawnTrail());

    }

    private void OnMouseUp()
    {
        if (_curBird != null)
        {
            _curBird.OnTap();

            TapCollider.enabled = false;
        }
    }

    //public void CheckGameEnd(GameObject destroyedEnemy) 
    public bool CheckGameEnd()//check game setiap kali burung mati aja...
    {
        // for (int i = 0; i < _enemies.Count; i++)
        // {
        //     if (_enemies[i].gameObject == destroyedEnemy)
        //     {
        //         _enemies.RemoveAt(i);
        //         Debug.Log(destroyedEnemy);
        //         Debug.Log(_enemies.Count);
        //         break;
        //     }
        // }
        // kalau mati bareng ngebug dia soalnya selain  karna list kan index berubah2... dia kayaknya gak bisa manggil bersamaan...

        //_enemyCounter--;//dicoba pake counter juga gak bisa kalau mati bareng... 
        //dicoba pake disable juga gak bisa... pake instance ajalah selanjutnya, event untuk hal yg hanya bisa dilakukan berurutan aja...


        //Debug.Log(_enemyCounter);
        int _enemyDeath = 0;
        for (int i = 0; i < _enemies.Count; i++)
        {
            if (_enemies[i].gameObject.activeSelf == false)
            {
                _enemyDeath++;

            }
        }

        Debug.Log(_enemyDeath);
        if (_enemyDeath == _enemyCounter)
        {
            _isGameEnded = true;
            _isWin = true;
            ShowEndGamePanel();
        }
        return (_isGameEnded);
    }

    public void ShowEndGamePanel()
    {
        EndGamePanel.SetActive(true);
        if (_isWin)
        {
            WinStatus.text = "You Win";
        }
        else
        {
            WinStatus.text = "You Lose";
        }
    }
    public void NextLevel()
    {

        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        Debug.Log(currentSceneIndex);
        if (currentSceneIndex < SceneManager.sceneCount)
        {
            SceneManager.LoadScene(currentSceneIndex + 1);
        }
        else
        SceneManager.LoadScene(0);

    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }


}
